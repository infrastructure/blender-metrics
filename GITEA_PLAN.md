Conversion Plan and Findings
============================

The gitea API seems to be complete enough to what we need:
https://future.projects.blender.org/api/swagger

Gitea has no transactions, but we can get all the events in the timeline query:
/repos/{owner}/{repo}/issues/{index}/timeline

The migrated data will most likely use comments for all the past events (e.g., change of priority).
That means we will need to have two ways of processing the data. We could do it based on the date
(using the migration day) or we could have two entries (comment or event) mapping to the same event.
At the moment I'm considering the latter.

That should cover (re-)fetching the old data.

For the new data we also need to update the SQL queries to map the old data to the new one.
For example, we will most likely stop with the classification process.

# TODOs for the future
[ ] Supported "deleted" issues/pull requests (sanitize the database every now and then by removing them).
[ ] calculate draft state when the pull request is created.

# Missing Features

1. Cursor
    We need a way to get the results of a query in a reliable way, even if the
    database is updated (new bugs are added or updated) while I'm fetching the
    subsequent pages of a query.

    So I can get a second "page" that doesn't contain the results from the first
    query and doesn't skip any results.

2. Modified After
    Similar to cursor (and perhaps part of the same solution) we need a way to only query
    issues and updates after a certain timestamp.

3. Sort query based on updated_at
    The /repos/{owner}/{repo}/issues returns the issues based on the ids.
# Bugs

1. closed_at not registered.

https://github.com/go-gitea/gitea/issues/22480
