import re

import numpy as np
import pandas as pd
import sqlalchemy as sa
from matplotlib import pyplot as plt


def main() -> None:
    with open('project_metrics.sql') as script, sa.create_engine(
        'sqlite:///data/data.sqlite'
    ).connect() as connection:
        for statement in script.read().split(';'):
            connection.execute(statement)

        projects = pd.read_sql_query(
            'SELECT * FROM recent_and_active_projects_with_metrics;',
            con=connection,
            parse_dates=['created', 'updated'],
        )
        projects['quarter'] = projects['created'].dt.to_period('Q')

        subscribers_with_metrics = pd.read_sql_query(
            'SELECT * FROM subscribers_with_metrics;', con=connection
        )

        users_per_quarter_and_node_type = pd.read_sql_query(
            'SELECT * FROM users_per_quarter_and_node_type;', con=connection
        )

        users_per_quarter_and_videos_watched = pd.read_sql_query(
            'SELECT * FROM users_per_quarter_and_videos_watched;', con=connection
        )

    plt.figure()
    nodes_per_project(projects)
    plt.figure()
    time_watched_per_subscriber(subscribers_with_metrics)
    plt.figure()
    videos_watched_per_subscriber(subscribers_with_metrics)

    plt.figure()
    videos_watched_over_time(users_per_quarter_and_videos_watched)
    plt.figure()
    user_nodes_over_time(users_per_quarter_and_node_type)
    plt.figure()
    projects_over_time(projects)


def nodes_per_project(projects: pd.DataFrame) -> None:
    xmin = projects['node_count'].min()
    xmax = projects['node_count'].max()

    ax = projects['node_count'].hist(
        bins=np.logspace(start=np.log10(xmin), stop=np.log10(xmax), num=200),
        cumulative=-1,
        histtype='step',
        density=True,
    )

    ax.set_xlabel('Node count')
    ax.set_xscale('log')
    ax.set_xlim(xmin, xmax)
    xticks = np.logspace(
        start=np.ceil(np.log2(xmin)),
        stop=np.floor(np.log2(xmax)),
        num=int(np.floor(np.log2(xmax)) - np.ceil(np.log2(xmin))) + 1,
        base=2,
    )
    ax.set_xticks(xticks)
    ax.set_xticklabels([f'{x:.0f}' for x in xticks])
    ax.set_xticks([], minor=True)

    ax.set_ylabel('Proportion of projects')
    ax.set_ylim(0, 1)
    ax.set_yticks(np.linspace(0.0, 1.0, 11))
    ax.set_yticks(np.linspace(0.0, 1.0, 41), minor=True)

    ax.set_axisbelow(True)
    ax.grid(True)

    ax.set_title('CDF of nodes per project for 2017-2019')

    ax.get_figure().set_tight_layout(True)
    ax.get_figure().set_size_inches(7 / 9 * 16, 7)
    ax.get_figure().savefig('figures/nodes_per_project.pdf')


def time_watched_per_subscriber(time_watched: pd.DataFrame) -> None:
    xmin = time_watched['time_watched_hours'].min()
    xmax = time_watched['time_watched_hours'].max()

    ax = time_watched['time_watched_hours'].hist(
        bins=[xmin] + list(np.logspace(start=np.log10(10 / 60), stop=np.log10(xmax), num=200)),
        cumulative=-1,
        histtype='step',
        density=True,
    )

    ax.set_xlabel('Time watched (hours)')
    ax.set_xscale('log')
    ax.set_xlim(10 / 60, xmax)
    xticks = [
        (10 / 60, '10 mins'),
        (15 / 60, '15 mins'),
        (30 / 60, '30 mins'),
        (1, '1 hour'),
        (2, '2 hours'),
        (4, '4 hours'),
        (8, '8 hours'),
        (14, '14 hours'),
        (24, '24 hours'),
    ]
    ax.set_xticks([x[0] for x in xticks])
    ax.set_xticklabels([x[1] for x in xticks])
    ax.xaxis.set_tick_params(rotation=90)
    ax.set_xticks([], minor=True)

    ax.set_ylabel('Proportion of subscribers')
    ax.set_ylim(0, 1)
    ax.set_yticks(np.linspace(0.0, 1.0, 11))
    ax.set_yticks(np.linspace(0.0, 1.0, 41), minor=True)

    ax.set_axisbelow(True)
    ax.grid(True)

    ax.set_title('CDF of time watched per subscriber for 2017-2019')

    ax.get_figure().set_tight_layout(True)
    ax.get_figure().set_size_inches(7 / 9 * 16, 7)
    ax.get_figure().savefig('figures/time_watched_per_subscriber.pdf')


def videos_watched_per_subscriber(videos_watched: pd.DataFrame) -> None:
    xmin = videos_watched['videos_watched'].min()
    xmax = videos_watched['videos_watched'].max()

    ax = videos_watched['videos_watched'].hist(
        bins=[xmin] + list(np.logspace(start=np.log10(max(xmin, 1)), stop=np.log10(xmax), num=200)),
        cumulative=-1,
        histtype='step',
        density=True,
    )

    ax.set_xlabel('Videos watched (> 50%)')
    ax.set_xscale('log')
    ax.set_xlim(max(xmin, 1), xmax)
    xticks = [xmin] + np.logspace(
        start=np.ceil(np.log2(max(xmin, 1))),
        stop=np.floor(np.log2(xmax)),
        num=int(np.floor(np.log2(xmax)) - np.ceil(np.log2(max(xmin, 1)))) + 1,
        base=2,
    )
    ax.set_xticks(xticks)
    ax.set_xticklabels([f'{x:.0f}' for x in xticks])
    ax.set_xticks([], minor=True)

    ax.set_ylabel('Proportion of subscribers')
    ax.set_ylim(0, 1)
    ax.set_yticks(np.linspace(0.0, 1.0, 11))
    ax.set_yticks(np.linspace(0.0, 1.0, 41), minor=True)

    ax.set_axisbelow(True)
    ax.grid(True)

    ax.set_title('CDF of videos watched per subscriber for 2017-2019')

    ax.get_figure().set_tight_layout(True)
    ax.get_figure().set_size_inches(7 / 9 * 16, 7)
    ax.get_figure().savefig('figures/videos_watched_per_subscriber.pdf')


def videos_watched_over_time(videos_watched: pd.DataFrame) -> None:
    df = videos_watched.rename(columns={'videos_watched': 'Videos watched'}).pivot(
        index='quarter', columns='Videos watched', values='users'
    )
    df = df[list(sorted(df.columns, key=lambda x: int(re.match(r'^(\d+)[-+]', x).group(1))))]
    ax = df.plot(kind='bar')

    ax.set_xlabel('Quarter')

    ax.set_ylabel('Users')
    xmax = df.max().max()
    ax.set_ylim(0, (xmax + 200) - ((xmax + 200) % 100))
    ax.set_yticks(np.arange(0, xmax + 200, 100))

    ax.set_axisbelow(True)
    ax.grid(True, axis='y')

    ax.set_title('Videos watched per user over 2017-2019')

    ax.get_figure().set_tight_layout(True)
    ax.get_figure().set_size_inches(7 / 9 * 16, 7)
    ax.get_figure().savefig('figures/videos_watched_over_time.pdf')


def user_nodes_over_time(user_nodes: pd.DataFrame) -> None:
    df = user_nodes.copy()

    df = (
        df.groupby(['quarter', 'node_type'])['users']
        .sum()
        .reset_index()
        .pivot(index='quarter', columns='node_type', values='users')
    )
    df = df[list(sorted(df.columns, key=lambda x: -df[x].max()))]
    ax = df.plot(kind='bar', legend=False)
    ax.legend(loc='upper center', ncol=5, frameon=True)
    ax.set_xlabel('Quarter')

    ax.set_ylabel('Users')
    xmax = df.max().max()
    ax.set_ylim(0, (xmax + 200) - ((xmax + 200) % 100))
    ax.set_yticks(np.arange(0, xmax + 200, 100))

    ax.set_axisbelow(True)
    ax.grid(True, axis='y')

    ax.set_title('Users per node type over 2017-2019')

    ax.get_figure().set_tight_layout(True)
    ax.get_figure().set_size_inches(7 / 9 * 16, 7)
    ax.get_figure().savefig('figures/user_nodes_over_time.pdf')


def projects_over_time(projects: pd.DataFrame) -> None:
    df = projects.copy()
    df['node_count'] = pd.cut(
        df['node_count'],
        [0, 20, 50, 100, 250, df['node_count'].max()],
        labels=['0-20', '21-50', '51-100', '101-250', '250+'],
    )
    df = (
        df.groupby(['quarter', 'node_count'])
        .size()
        .reset_index()
        .pivot(index='quarter', columns='node_count', values=0)
    )

    ax = df.plot(kind='bar', stacked=True, legend=False)

    ax.legend(title='Node count')

    ax.set_xlabel('Quarter')

    ax.set_ylabel('Projects')
    xmax = df.max().sum()
    ax.set_ylim(0, (xmax + 2) - ((xmax + 2) % 1))
    ax.set_yticks(np.arange(0, xmax + 2, 1))

    ax.set_axisbelow(True)
    ax.grid(True, axis='y')

    ax.set_title('Number of projects over 2017-2019')

    ax.get_figure().set_tight_layout(True)
    ax.get_figure().set_size_inches(7 / 9 * 16, 7)
    ax.get_figure().savefig('figures/projects_over_time.pdf')


if __name__ == '__main__':
    main()
