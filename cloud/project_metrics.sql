CREATE INDEX IF NOT EXISTS users__id ON users (id);
CREATE INDEX IF NOT EXISTS users_roles__user_id_role ON users_roles (user_id, role);
CREATE INDEX IF NOT EXISTS users_view_progress__user_id ON users_view_progress (user_id);
CREATE INDEX IF NOT EXISTS nodes__project_id ON nodes (project_id);
CREATE INDEX IF NOT EXISTS projects_groups__project_id ON projects_groups (project_id);
CREATE INDEX IF NOT EXISTS users_groups__group_id ON users_groups (group_id);

CREATE TEMPORARY TABLE recent_and_active_projects_with_metrics AS
SELECT *,
       (SELECT count(*) FROM nodes WHERE nodes.project_id = projects.id) AS node_count,
       (SELECT count(*)
        FROM nodes
        WHERE nodes.project_id = projects.id
          AND nodes.node_type LIKE 'attract_%')                          AS attract_node_count,
       (SELECT count(*)
        FROM projects_groups
                 LEFT JOIN users_groups on projects_groups.group_id = users_groups.group_id
        WHERE projects_groups.project_id = projects.id)                  AS user_count
FROM projects
WHERE projects.is_private
  AND projects.category != 'home'
  AND cast(strftime('%Y', projects.created) AS INTEGER) >= 2017
  AND cast(strftime('%Y', projects.created) AS INTEGER) < 2020
  AND exists(SELECT *
             FROM nodes
             WHERE nodes.project_id = projects.id
               AND julianday(nodes.updated) - julianday(projects.created) > 7)
  AND (SELECT count(*) FROM nodes WHERE nodes.project_id = projects.id) > 10;

CREATE TEMPORARY TABLE subscribers_with_metrics AS
SELECT u.id,
       coalesce((SELECT sum(uvp.progress_in_sec / 3600)
                 FROM users_view_progress uvp
                 WHERE uvp.user_id = u.id), 0)           AS time_watched_hours,
       coalesce((SELECT count(*)
                 FROM users_view_progress uvp
                 WHERE uvp.user_id = u.id
                   AND uvp.progress_in_percent > 50), 0) AS videos_watched
FROM users u
WHERE exists(SELECT * FROM users_roles ur WHERE ur.user_id = u.id AND ur.role = 'subscriber');

-- TODO(sem): Only use users which were ever a subscriber?
CREATE TEMPORARY TABLE users_per_quarter_and_node_type AS
SELECT (strftime('%Y', nodes.created) || 'Q' ||
        (cast(((cast(strftime('%m', nodes.created) AS INTEGER) - 1) / 3) AS INTEGER) +
         1))                         AS quarter,
       (CASE
            WHEN nodes.name in
                 ('Blender Sync', 'userpref.blend', 'startup.blend', '2.77', '2.78', '2.79', '2.80',
                  '2.81', '2.82') THEN 'Sync'
            WHEN nodes.node_type LIKE 'attract%' THEN 'Attract'
            WHEN nodes.node_type = 'asset' THEN 'Asset'
            WHEN nodes.node_type = 'comment' THEN 'Comment'
            ELSE 'Other'
           END)                      AS node_type,
       count(distinct nodes.user_id) AS users
FROM nodes
WHERE cast(strftime('%Y', nodes.created) AS INTEGER) >= 2017
  AND cast(strftime('%Y', nodes.created) AS INTEGER) < 2020
GROUP BY (strftime('%Y', nodes.created) || 'Q' ||
          (cast(((cast(strftime('%m', nodes.created) AS INTEGER) - 1) / 3) AS INTEGER) +
           1)),
         (CASE
              WHEN nodes.name in
                   ('Blender Sync', 'userpref.blend', 'startup.blend', '2.77', '2.78', '2.79',
                    '2.80',
                    '2.81', '2.82') THEN 'Sync'
              WHEN nodes.node_type LIKE 'attract%' THEN 'Attract'
              WHEN nodes.node_type = 'asset' THEN 'Asset'
              WHEN nodes.node_type = 'comment' THEN 'Comment'
              ELSE 'Other'
             END);

CREATE TEMPORARY TABLE users_per_quarter_and_videos_watched AS
SELECT uvp.quarter, uvp.videos_watched, count(*) AS users
FROM (
         SELECT (strftime('%Y', uvp.last_watched) || 'Q' ||
                 (cast(((cast(strftime('%m', uvp.last_watched) AS INTEGER) - 1) / 3) AS INTEGER) +
                  1))                AS quarter,
                uvp.user_id,
                (CASE
                     WHEN count(*) <= 5 THEN '0-5'
                     WHEN count(*) <= 10 THEN '6-10'
                     WHEN count(*) <= 20 THEN '11-20'
                     WHEN count(*) <= 50 THEN '21-50'
                     ELSE '51+' END) AS videos_watched
         FROM users_view_progress uvp
         WHERE cast(strftime('%Y', uvp.last_watched) AS INTEGER) >= 2017
           AND cast(strftime('%Y', uvp.last_watched) AS INTEGER) < 2020
           AND uvp.progress_in_percent > 50
         GROUP BY (strftime('%Y', uvp.last_watched) || 'Q' ||
                   (cast(((cast(strftime('%m', uvp.last_watched) AS INTEGER) - 1) / 3) AS INTEGER) +
                    1)), uvp.user_id
     ) uvp
GROUP BY uvp.quarter, uvp.videos_watched;
