import dataclasses as dc
import datetime
from typing import (
    Callable,
    Dict,
    Iterable,
    List,
    NewType,
    Optional,
    Tuple,
    TypeVar,
    cast,
)


@dc.dataclass
class Transaction:
    id: int
    issue_number: int
    author_id: int
    date_created: datetime.datetime
    date_modified: Optional[datetime.datetime]


@dc.dataclass
class LabelTransaction(Transaction):
    is_set: bool
    label: str


@dc.dataclass
class ProjectTransaction(Transaction):
    old: int
    new: int


@dc.dataclass
class CloseTransaction(Transaction):
    """
    This includes automatically closed by phabricator or manually by a developer.
    """
    pass


@dc.dataclass
class MergePullTransaction(Transaction):
    pass


@dc.dataclass
class DraftTransaction(Transaction):
    is_set: bool


@dc.dataclass
class NullTransaction(Transaction):
    """
    Old transactions that we are not parsing or transactions that we are ignoring.
    """
    pass


@dc.dataclass
class PriorityTransaction(Transaction):
    priority: str


@dc.dataclass
class SubtypeTransaction(Transaction):
    subtype: str
    is_set: bool


@dc.dataclass
class StatusTransaction(Transaction):
    status: str
    is_set: bool


@dc.dataclass
class Moderator:
    id: int


@dc.dataclass
class Issue:
    number: int
    author_id: int
    state: str
    label_type: str
    label_status: str
    label_severity: str
    date_created: datetime.datetime
    date_modified: datetime.datetime
    date_closed: Optional[datetime.datetime]
    title: str
    api_type: str
    created_as_draft: Optional[bool]
