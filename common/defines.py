import datetime


class PRIORITY:
    NORMAL = "normal"
    HIGH = "high"
    UNBREAK = "unbreak"


class SUBTYPE:
    BUG = "bug"
    DESIGN = "design"
    TODO = "todo"

class MIGRATION:
    STANDIN_AUTHOR = -1
    DATE = datetime.datetime(2023, 2, 7)