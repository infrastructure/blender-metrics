
import logging
import requests
import sys
import yarl
from urllib.parse import urlparse, parse_qs
from retry import retry as retry_decorator
import datetime

from .utils import (
    assert_cast,
    assert_cast_optional,
    get_date_object,
    get_date_string,
    is_draft_title,
)

from .classes import *

from .classes import (
    Moderator,
    Issue,
)

from typing import (
    Callable,
    Dict,
    Iterable,
    List,
    NewType,
    Optional,
    Tuple,
    TypeVar,
    cast,
)


logger = logging.getLogger(__file__)

Page = NewType('Page', int)

F = TypeVar('F', bound=Callable[..., object])

retry: Callable[[F], F] = retry_decorator(
    tries=10, delay=1, backoff=2, logger=logger)


def get_next_page(headers) -> int:
    """
    Parse the header looking for reference to next.
    """
    try:
        link = headers['link']
        link_parts = link.split(',')

        next_url = None

        for link_part in link_parts:
            link_subparts = link_part.split(';')
            if len(link_subparts) != 2:
                continue
            if link_subparts[1] != ' rel="next"':
                continue
            # Cleanup the string into a proper URL
            next_url = link_subparts[0][1:-1]

        if next_url is None:
            return None

        parsed_url = urlparse(next_url)
        query = parsed_url.query
        return int(parse_qs(query)['page'][0])
    except:
        return None


@retry
def fetch_single(
    api_url: yarl.URL,
    api_token: str,
    method: str,
    data: Dict[str, str],
    page: Optional[Page] = None,
) -> Tuple[List[object], Optional[Page]]:
    """Generic function to query a single item from the API.

    Returns:
        A dictionary containing the item data.
    """
    headers = {
        'accept': 'application/json',
    }

    params = {
        'token': api_token,
        **data,
    }

    if page is not None:
        params['page'] = page

    logger.info(f"Calling {method} ({params=}, {headers=}).")
    response = requests.get(str(api_url / method), params=params, headers=headers)
    response.raise_for_status()
    response_json = response.json()

    next_page = get_next_page(response.headers)
    return response_json, None if next_page is None else Page(next_page)


def fetch_all(
    api_url: yarl.URL,
    api_token: str,
    method: str,
    data: Dict[str, str],
) -> Iterable[object]:
    """Generic function to query lists from API.

    Yields:
        response_data - the result of fetch_single()
    """
    response_data, page = fetch_single(api_url, api_token, method, data)
    yield from response_data if response_data is not None else ()
    while page is not None:
        response_data, page = fetch_single(
            api_url, api_token, method, data, page=page)
        yield from response_data


def fetch_moderators(
    api_url: yarl.URL,
    api_token: str,
    organization_name: str,
    moderators_team_name: str,
) -> List[Moderator]:
    """Query API for all the members of a project

    This is used to filter out issues created by "the community"
    and by the development team(volunteers and paid developers).

    Yields:
        Moderator objects.
    """

    method = "orgs/{org}/teams".format(org=organization_name)
    moderators_id = None

    for team in cast(
        Iterable[Dict[object, object]],
        fetch_all(
            api_url,
            api_token,
            method,
            data={},
        ),
    ):
        if team.get('name') != moderators_team_name:
            continue

        moderators_id = team.get('id')
        break

    if moderators_id is None:
        logger.error('No moderator team found.')
        sys.exit(2)

    method = "teams/{id}/members".format(id=moderators_id)
    moderators = list()

    for member in cast(
        Iterable[Dict[object, object]],
        fetch_all(
            api_url,
            api_token,
            method,
            data={},
        ),
    ):
        member_id = member.get('id')
        moderators.append(Moderator(id=member_id))

    return moderators


def fetch_issues(
    api_url: yarl.URL,
    api_token: str,
    repository_owner: str,
    repository_name: str,
    IteratorClass: Issue,
    after: Optional[datetime.datetime] = None,
) -> Iterable[Issue]:
    """Query API for all issues that changed since "after".

    The query is not limited by any project.

    Yields:
        Issue objects.
    """
    modified_start: Dict[str, str]
    if after:
        modified_start = {
            'since': get_date_string(after)
            }
    else:
        modified_start = {}

    method = "repos/{owner}/{repo}/issues".format(owner=repository_owner, repo=repository_name)

    for issue in cast(
        Iterable[Dict[object, object]],
        fetch_all(
            api_url,
            api_token,
            method,
            data={
                'state': 'all',
                'type': IteratorClass.api_type,
                # 'order[0]': '-updated',
                # 'order[1]': '-id',
                **modified_start,
            },
        ),
    ):
        logger.info('issue: number={number}'.format(**issue))

        # Initialize label fields.
        label_type = ""
        label_status = ""
        label_severity = ""

        # Populate label fields based on the labels list
        for label in issue.get('labels', []):
            label_name = label.get('name', "")
            if label_name.startswith("Type/"):
                label_type = label_name
            elif label_name.startswith("Status/"):
                label_status = label_name
            elif label_name.startswith("Severity/"):
                label_severity = label_name

        yield IteratorClass(
            number=assert_cast(int, issue.get('number')),
            author_id=assert_cast(int, assert_cast(
                dict, issue.get('user', {})).get('id')),
            state=assert_cast(str, issue.get('state')),
            title=assert_cast(str, issue.get('title')),
            date_created=get_date_object(
                assert_cast(str, issue.get('created_at'))
            ),
            date_modified=get_date_object(
                assert_cast(str, issue.get('updated_at'))
            ),
            date_closed=(
                get_date_object(
                    assert_cast(str, issue.get('closed_at')))
                if issue.get('closed_at') is not None
                else None
            ),
            label_type=label_type,
            label_status=label_status,
            label_severity=label_severity,
        )


def PotentialDraftFromTitleTransaction(
    kwargs: Dict,
    transaction: Iterable[Dict[object, object]],
) -> Transaction:
    """
    Parse the title transaction to see if there is a draft status change.
    """
    old_title = assert_cast(str, transaction.get('old_title'))
    new_title = assert_cast(str, transaction.get('new_title'))

    was_draft = is_draft_title(old_title)
    is_draft = is_draft_title(new_title)

    if is_draft != was_draft:
        return DraftTransaction(
            is_set=is_draft,
            **kwargs,
        )

    return NullTransaction(**kwargs)


def fetch_transactions(
    api_url: yarl.URL,
    api_token: str,
    repository_owner: str,
    repository_name: str,
    issue_number: int,
) -> Iterable[Transaction]:
    """Query API for all the transactions that ever happened to a Diff."""

    # Note: Gitea uses the number, not the ID for the API
    method = "repos/{owner}/{repo}/issues/{index}/timeline".format(
        owner=repository_owner,
        repo=repository_name,
        index=issue_number,
    )

    for transaction in cast(
        Iterable[Dict[object, object]],
        fetch_all(
            api_url,
            api_token,
            method,
            data={},
        ),
    ):
        # This should not happen, however it does sometimes:
        # https://projects.blender.org/infrastructure/blender-projects-platform/issues/76
        if transaction is None:
            continue
        transaction_type = assert_cast_optional(str, transaction.get('type'))

        kwargs = Transaction(
            id=assert_cast(int, transaction.get('id')),
            issue_number=issue_number,
            author_id=assert_cast(int, assert_cast(
                dict, transaction.get('user', {})).get('id')),
            date_created=get_date_object(
                assert_cast(str, transaction.get('created_at'))
            ),
            date_modified=(
                get_date_object(
                    assert_cast(str, transaction.get('date_modified')))
                if transaction.get('date_modified') is not None
                else None
            ),
        )

        logger.debug("transaction_id={id}, type={type}".format(**transaction))
        if transaction_type == 'pull_push':
            # The pull request was just created.
            yield NullTransaction(**dc.asdict(kwargs))
        elif transaction_type == 'close':
            # The pull request was manually closed.
            yield CloseTransaction(**dc.asdict(kwargs))
        elif transaction_type == 'merge_pull':
            # The pull request was merged.
            yield MergePullTransaction(**dc.asdict(kwargs))
        elif transaction_type == 'label':
            # A label was added or removed.
            yield LabelTransaction(
                is_set=assert_cast(str, transaction.get('body')) == "1",
                label=assert_cast(str, assert_cast(
                    dict, transaction.get('label', {})).get('name')),
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'project':
            # A project was set or unset
            yield ProjectTransaction(
                old=assert_cast(int, transaction.get('old_project_id')),
                new=assert_cast(int, transaction.get('project_id')),
                **dc.asdict(kwargs),
            )
        elif transaction_type == 'change_title':
            # It can be either a legit title change, or changing the draft state.
            yield PotentialDraftFromTitleTransaction(dc.asdict(kwargs), transaction)
        else:
            # Failsafe for now, it could be removed later so we make sure
            # that all the transaction types are accounted for one way or another
            yield NullTransaction(**dc.asdict(kwargs))
