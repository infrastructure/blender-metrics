
from contextlib import closing
import datetime
import sqlite3
from typing import Iterable
from more_itertools import chunked

from .utils import (
    assert_cast_optional,
    get_date_object,
)

from .classes import *


def get_max_date_modified(connection: sqlite3.Connection) -> Optional[datetime.datetime]:
    """Get the date of the latest diff entry in the database.

    Returns:
        An optional datetime object.
    """
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        cursor.execute(
            '''
            SELECT coalesce(issues.date_modified, issues.date_created)
            FROM issues
            ORDER BY coalesce(issues.date_modified, issues.date_created) DESC
            LIMIT 1;
            '''
        )
        result = cursor.fetchall()

    if not result:
        return None
    else:
        return get_date_object(result[0][0])


def init_sqlite(connection: sqlite3.Connection) -> None:
    connection.executescript(
        '''
        PRAGMA foreign_keys = ON;
        PRAGMA encoding = 'UTF-8';
        '''
    )
    connection.executescript(
        '''
        DROP table IF EXISTS moderators;

        CREATE TABLE moderators
        (
            id              TEXT NOT NULL
        );

        CREATE UNIQUE INDEX IF NOT EXISTS moderators__id
            ON moderators (id);

        CREATE TABLE IF NOT EXISTS issues
        (
            number            BIGINT NOT NULL,
            state             TEXT NOT NULL,
            label_status      TEXT,
            label_type        TEXT,
            label_severity    TEXT,
            created_as_draft  INT NOT NULL,
            author_id         BIGINT NOT NULL,
            date_created      DATETIME NOT NULL,
            date_modified     DATETIME NOT NULL,
            date_closed       DATETIME
        );

        CREATE UNIQUE INDEX IF NOT EXISTS issues__number
            ON issues (number);

        CREATE INDEX IF NOT EXISTS issues__date_created
            ON issues (date_created);

        CREATE INDEX IF NOT EXISTS issues__date_modified
            ON issues (date_modified);

        CREATE TABLE IF NOT EXISTS transactions
        (
            id            BIGINT NOT NULL,
            issue_number  BIGINT NOT NULL REFERENCES issues(number),
            author_id     BIGINT NOT NULL,
            date_created  DATETIME NOT NULL,
            date_modified DATETIME
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions__id
            ON transactions (id);

        CREATE INDEX IF NOT EXISTS transactions_issue_number
            ON transactions (issue_number);

        CREATE INDEX IF NOT EXISTS transactions__author_id
            ON transactions (author_id);

        CREATE INDEX IF NOT EXISTS transactions__first_update
            ON transactions(issue_number, id, date_created);

        CREATE TABLE IF NOT EXISTS transactions_label
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            label            TEXT NOT NULL,
            is_set           INT NOT NULL
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_label__transaction_id
            ON transactions_label (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_merge_pull
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id)
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_merge_pull__transaction_id
            ON transactions_merge_pull (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_draft
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            is_set         INT NOT NULL
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_draft__transaction_id
            ON transactions_draft (transaction_id);


        CREATE TABLE IF NOT EXISTS transactions_close
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id)
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_close__transaction_id
            ON transactions_close (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_project
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id),
            old            INT NOT NULL,
            new            INT NOT NULL
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_project__transaction_id
            ON transactions_project (transaction_id);

        CREATE TABLE IF NOT EXISTS transactions_null
        (
            transaction_id BIGINT NOT NULL REFERENCES transactions(id)
        );

        CREATE UNIQUE INDEX IF NOT EXISTS transactions_null__transaction_id
            ON transactions_null (transaction_id);

        '''
    )


def store_issue_and_transactions(
    issue: Issue, transactions: Iterable[Transaction], connection: sqlite3.Connection
) -> None:
    """Save a diff and its transactions, overriding existing entries."""
    cursor: sqlite3.Cursor
    with connection:
        connection.execute(
            '''
            INSERT INTO issues (
                number,
                author_id,
                state,
                label_status,
                label_type,
                label_severity,
                created_as_draft,
                date_created,
                date_modified,
                date_closed
            )
            VALUES (
                :number,
                :author_id,
                :state,
                :label_status,
                :label_type,
                :label_severity,
                :created_as_draft,
                :date_created,
                :date_modified,
                :date_closed
            )
            ON CONFLICT (number) DO UPDATE SET
                author_id= :author_id,
                state= :state,
                label_status= :label_status,
                label_type= :label_type,
                label_severity= :label_severity,
                created_as_draft= :created_as_draft,
                date_created= :date_created,
                date_modified= :date_modified,
                date_closed= :date_closed;
            ''',
            dc.asdict(issue),
        )

        for chunk in chunked(transactions, n=256):
            connection.executemany(
                '''
                INSERT INTO transactions (
                    id,
                    issue_number,
                    author_id,
                    date_created,
                    date_modified
                )
                VALUES (
                    :id,
                    :issue_number,
                    :author_id,
                    :date_created,
                    :date_modified
                )
                ON CONFLICT (id) DO UPDATE SET
                    issue_number= :issue_number,
                    author_id= :author_id,
                    date_created= :date_created,
                    date_modified= :date_modified;
                ''',
                [
                    {
                        'id': transaction.id,
                        'issue_number': transaction.issue_number,
                        'author_id': transaction.author_id,
                        'date_created': transaction.date_created,
                        'date_modified': transaction.date_modified,
                    }
                    for transaction in chunk
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_label (
                    transaction_id,
                    is_set,
                    label
                )
                VALUES (
                    :transaction_id,
                    :is_set,
                    :label
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    is_set= :is_set,
                    label= :label;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'is_set': transaction.is_set,
                        'label': transaction.label,
                    }
                    for transaction in chunk
                    if isinstance(transaction, LabelTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_project (
                    transaction_id,
                    old,
                    new
                )
                VALUES (
                    :transaction_id,
                    :old,
                    :new
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    old= :old,
                    new= :new;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'old': transaction.old,
                        'new': transaction.new,
                    }
                    for transaction in chunk
                    if isinstance(transaction, ProjectTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_close (
                    transaction_id
                )
                VALUES (
                    :transaction_id
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    transaction_id= :transaction_id;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                    }
                    for transaction in chunk
                    if isinstance(transaction, CloseTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_merge_pull (
                    transaction_id
                )
                VALUES (
                    :transaction_id
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    transaction_id= :transaction_id;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                    }
                    for transaction in chunk
                    if isinstance(transaction, MergePullTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_draft (
                    transaction_id,
                    is_set
                )
                VALUES (
                    :transaction_id,
                    :is_set
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    is_set= :is_set
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                        'is_set': transaction.is_set,
                    }
                    for transaction in chunk
                    if isinstance(transaction, DraftTransaction)
                ],
            )

            connection.executemany(
                '''
                INSERT INTO transactions_null (
                    transaction_id
                )
                VALUES (
                    :transaction_id
                )
                ON CONFLICT (transaction_id) DO UPDATE SET
                    transaction_id= :transaction_id;
                ''',
                [
                    {
                        'transaction_id': transaction.id,
                    }
                    for transaction in chunk
                    if isinstance(transaction, NullTransaction)
                ],
            )
