
import logging
import sqlite3
from more_itertools import chunked

from .classes import (
    Moderator,
)

from typing import (
    Callable,
    Dict,
    Iterable,
    List,
    NewType,
    Optional,
    Tuple,
    TypeVar,
    cast,
)

logger = logging.getLogger(__file__)


def store_moderators(
    moderators: Iterable[Moderator], connection: sqlite3.Connection
) -> None:
    """Save the latest moderators list, deleting all existing entries."""
    cursor: sqlite3.Cursor

    logger.info(f'Storing {len(moderators)} moderators.')

    with connection:
        for chunk in chunked(moderators, n=256):
            connection.executemany(
                '''
                INSERT INTO moderators (
                    id
                )
                VALUES (
                    :id
                )
                ''',
                [
                    {
                        'id': moderator.id,
                    }
                    for moderator in chunk
                ],
            )
