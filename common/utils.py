from contextlib import closing
import logging
import sqlite3
import sys
import os
from pathlib import Path
from typing import Type, cast, Optional, TypeVar
import datetime
import rfc3339
import iso8601


T = TypeVar('T', bound=object)
data_dir = Path(__file__).parent.parent / 'data'


def assert_cast(typ: Type[T], obj: object) -> T:
    assert isinstance(obj, typ), f'object is not of type {typ}: {obj}'
    return cast(T, obj)


def assert_cast_optional(typ: Type[T], obj: Optional[object]) -> Optional[T]:
    if obj is None:
        return obj
    else:
        return assert_cast(typ, obj)


def setup_logging(logger: logging.Logger, filename: str) -> None:
    Path('data').mkdir(exist_ok=True)

    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    file_handler = logging.FileHandler(str(data_dir / filename))
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    stream_handler = logging.StreamHandler(sys.stderr)
    stream_handler.setLevel(logging.ERROR)
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(logging.Formatter('%(message)s'))
    logger.addHandler(stream_handler)


def blender_git_repository_get(dirname: str) -> str:
    """Return a valid directory for a local git repository folder"""

    # The test for a valid git repository happens on argument parsing.
    if not dirname:
        return ""

    dirname = os.path.expanduser(dirname)

    if not os.path.isdir(dirname):
        logger.error("The specified repository is not a folder in the system: " + dirname)
        sys.exit(9)

    if not os.access(dirname, os.R_OK):
        logger.error("Cannot read repository: " + dirname)
        sys.exit(3)

    return dirname


def is_draft_title(title: str) -> bool:
    """
    Returns whether the title makes the pull issue a draft.
    """
    title_lower = title.lower()

    if title_lower.startswith("wip:"):
        return True

    if title_lower.startswith("[wip]"):
        return True

    return False


def get_date_object(date_string: str) -> datetime.datetime:
  return iso8601.parse_date(date_string)


def get_date_string(date_object: datetime.datetime) -> str:
  return rfc3339.rfc3339(date_object)

