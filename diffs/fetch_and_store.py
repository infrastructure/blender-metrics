#!/usr/bin/env python3.8

"""Fetch and store Diffs and Transactions from developer.blender.org.

Needs to be run as module with `python -m diffs.fetch_and_store`.
This script allows to query the API (Gitea) of the Blender
development portal and fetch Differentials and their relative transactions.

The data is progressively fetched and saved into a local sqlite database,
for more efficient processing and data mining through other scripts.

The script works as follows:

    * main() is the entry point
    * fetch diffs from the server starting from `after`
        * fetch the transactions for each diff
        * save diff its transactions in the sqlite database
"""

import dataclasses as dc
import datetime
import logging
import os
import sqlite3
import sys
import yarl
from more_itertools import chunked
from contextlib import closing
from common.utils import (
    assert_cast_optional,
    setup_logging,
    is_draft_title,
    data_dir,
    get_date_object,
)
from common.classes import *
from common.fetch import (
    fetch_issues,
    fetch_moderators,
    fetch_transactions,
)
from common.store import store_moderators
from typing import (
    Dict,
    Iterable,
    NewType,
    Optional,
    cast,
)
from common.settings import *
from common.sql import (
    get_max_date_modified,
    init_sqlite,
    store_issue_and_transactions,
)


logger = logging.getLogger(__file__)


@dc.dataclass
class Diff (Issue):
    api_type = "pulls"

    def __init__(self, **kwargs):
        Issue.__init__(
            self,
            api_type=self.api_type,
            created_as_draft=is_draft_title(kwargs.get('title')),
            **kwargs,
        )


def fetch_diffs(
    api_url: yarl.URL,
    api_token: str,
    repository_owner: str,
    repository_name: str,
    after: Optional[datetime.datetime] = None,
) -> Iterable[Diff]:
    """Query API for all diffs that changed since "after".

    The query is not limited by any project.

    Yields:
        Diff objects.
    """
    return fetch_issues(**locals(), IteratorClass=Diff)


def set_created_as_draft(diff: Diff, transactions: List[Transaction]):
    """
    Set the the diff draft state based on the transactions.
    If there was no draft transaction we return None
    In this case the title of the diff was already used to determine that.
    """
    for transaction in transactions:
        if not isinstance(transaction, DraftTransaction):
            continue

        diff.created_as_draft = not transaction.is_set
        return


def main() -> None:
    api_token = os.environ['GITEA_API_TOKEN']

    data_dir.mkdir(exist_ok=True)

    setup_logging(logger, 'log-diff.txt')

    gitea_domain = GITEA_DOMAIN
    api_url = yarl.URL(gitea_domain + 'api/v1/')
    organization_name = ORGANIZATION_NAME
    repository_owner = REPOSITORY_OWNER
    repository_name = REPOSITORY_NAME
    moderators_team_name = MODERATORS_TEAM_NAME

    with closing(sqlite3.connect(str(data_dir / 'diffs.sqlite'))) as connection:
        init_sqlite(connection)

        after: Optional[datetime.datetime]
        if len(sys.argv) > 1 and sys.argv[1]:
            after = get_date_object(sys.argv[1])
        else:
            after = get_max_date_modified(connection)

        if after is not None:
            logger.info(f'Fetching updates after: {after.isoformat(sep=" ")}')

        for i, diff in enumerate(
            fetch_diffs(api_url, api_token, repository_owner, repository_name, after=after)
        ):
            logger.info("Iterating over diff.number=" + str(diff.number))
            transactions = fetch_transactions(
                api_url,
                api_token,
                repository_owner,
                repository_name,
                issue_number=diff.number)

            # TODO Disabled set_create_as_draft for now since we can't run over the iterator duh!
            # set_created_as_draft(diff, transactions)
            store_issue_and_transactions(diff, transactions, connection)

            # Report update every now and then
            if i % 10 == 0:
                current_after = diff.date_modified

                if current_after is None:
                    logger.info(f'Fetched {i + 1} diffs.')
                else:
                    logger.info(
                        f'Fetched {i + 1} diffs. We now have all modifications after: {current_after.isoformat(sep=" ")}'
                    )

        # Get a new moderators list every time
        logger.info(f'Fetching moderators list')
        moderators = fetch_moderators(
            api_url, api_token, organization_name, moderators_team_name)
        store_moderators(moderators, connection)

        # TODO set create_as_draft and update SQL with this info
        # For now we assume that if a patch has WIP in its title
        # that it most likely is a draft still


if __name__ == '__main__':
    main()
