#!/usr/bin/env python3.8

from argparse import ArgumentParser
import dataclasses as dc
import datetime
import logging
import sqlite3
import sys
import tempfile
import csv
import os
from contextlib import closing
from pathlib import Path
from typing import List, Optional
from enum import Enum

from common.utils import assert_cast_optional, setup_logging, data_dir, T

logger = logging.getLogger(__file__)


@dc.dataclass
class InputArguments:
    query: Enum
    date_start: datetime.datetime
    date_end: datetime.datetime
    output: str


class Query(Enum):
    NEW_DIFFS_ALL = 'new'
    NEW_DIFFS_MODERATORS = 'new_moderators'
    NEW_DIFFS_COMMUNITY = 'new_community'
    OPEN_DIFFS = 'open'
    OPEN_DIFFS_COMMUNITY = 'open_community'
    OPEN_DIFFS_DRAFT = 'draft'
    OPEN_DIFFS_DRAFT_COMMUNITY = 'draft_community'
    OPEN_DIFFS_NEEDS_REVIEW = 'needs_review'
    OPEN_DIFFS_NEEDS_REVIEW_COMMUNITY = 'needs_review_community'
    CLOSED_DIFFS_ALL = 'closed'
    CLOSED_DIFFS_ALL_COMMUNITY = 'closed_community'
    CLOSED_DIFFS_MERGED = 'merged'
    CLOSED_DIFFS_MERGED_COMMUNITY = 'merged_community'
    CLOSED_DIFFS_ABANDONED = 'abandoned'
    CLOSED_DIFFS_ABANDONED_COMMUNITY = 'abandoned_community'
    DEBUG_RESET = 'reset'

    def __str__(self):
        return self.value


@dc.dataclass
class Diff:
    pass


@dc.dataclass
class Day:
    date: datetime.datetime
    query: Enum
    total: int = 0

    def __str__(self):
        return "{date};{total};".format(date=self.date.strftime("%Y-%m-%d"), total=self.total)

    def __iter__(self):
        yield from (self.date.strftime("%Y-%m-%d"), self.total)

    def process(self, connection: sqlite3.Connection) -> None:
        if self.query == Query.NEW_DIFFS_ALL:
            self.total = get_new_diffs(connection, date=self.date, is_moderator=True, is_community=True)
        elif self.query == Query.NEW_DIFFS_MODERATORS:
            self.total = get_new_diffs(connection, date=self.date, is_moderator=True, is_community=False)
        elif self.query == Query.NEW_DIFFS_COMMUNITY:
            self.total = get_new_diffs(connection, date=self.date, is_moderator=False, is_community=True)
        elif self.query == Query.OPEN_DIFFS:
            self.total = get_open_diffs(connection, date=self.date, is_community=False)
        elif self.query == Query.OPEN_DIFFS_COMMUNITY:
            self.total = get_open_diffs(connection, date=self.date, is_community=True)
        elif self.query == Query.OPEN_DIFFS_DRAFT:
            self.total = get_open_diffs_extra(
                connection,
                self.date,
                is_draft=True,
                is_community=False,
            )
        elif self.query == Query.OPEN_DIFFS_DRAFT_COMMUNITY:
            self.total = get_open_diffs_extra(
                connection,
                self.date,
                is_draft=True,
                is_community=True,
            )
        elif self.query == Query.OPEN_DIFFS_NEEDS_REVIEW:
            self.total = get_open_diffs_extra(
                connection,
                self.date,
                is_draft=False,
                is_community=False,
            )
        elif self.query == Query.OPEN_DIFFS_NEEDS_REVIEW_COMMUNITY:
            self.total = get_open_diffs_extra(
                connection,
                self.date,
                is_draft=False,
                is_community=True,
            )
        elif self.query == Query.CLOSED_DIFFS_ALL:
            self.total = get_closed_diffs(
                connection,
                self.date,
                is_community=False,
            )
        elif self.query == Query.CLOSED_DIFFS_ALL_COMMUNITY:
            self.total = get_closed_diffs(
                connection,
                self.date,
                is_community=True,
            )
        elif self.query == Query.CLOSED_DIFFS_MERGED:
            self.total = get_closed_diffs_extra(
                connection,
                self.date,
                is_abandoned=False,
                is_merged=True,
                is_community=False,
            )
        elif self.query == Query.CLOSED_DIFFS_MERGED_COMMUNITY:
            self.total = get_closed_diffs_extra(
                connection,
                self.date,
                is_abandoned=False,
                is_merged=True,
                is_community=True,
            )
        elif self.query == Query.CLOSED_DIFFS_ABANDONED:
            self.total = get_closed_diffs_extra(
                connection,
                self.date,
                is_abandoned=True,
                is_merged=False,
                is_community=False,
            )
        elif self.query == Query.CLOSED_DIFFS_ABANDONED_COMMUNITY:
            self.total = get_closed_diffs_extra(
                connection,
                self.date,
                is_abandoned=True,
                is_merged=False,
                is_community=True,
            )
        else:
            logger.error("query type not fully implemented: %s" %
                         (self.query,))


def process_arguments() -> InputArguments:
    """Mode is mandatory, date range is optional."""
    parser = ArgumentParser(
        description='''
    Count tracker items for a range of days.
    By default will get today's data, for tasks that need developers attention.
    '''
    )
    parser.add_argument(
        'query',
        type=Query,
        choices=list(Query),
        help='''
        query parameters: open, new, closed patches
        ''',
    )
    parser.add_argument(
        '--start',
        type=datetime.datetime.fromisoformat,
        help='''
        first day of range, today as default
        ''',
    )
    parser.add_argument(
        '--end',
        type=datetime.datetime.fromisoformat,
        help='''
        last day of range, optional, today as default
        ''',
    )
    parser.add_argument(
        '--output',
        type=str,
        default="",
        help='''
        optional output csv file
        ''',
    )

    arguments_raw = parser.parse_args()

    if arguments_raw.start is None:
        if arguments_raw.end is not None:
            parser.error(
                "The --end argument can only be used when --start is specified")
        else:
            arguments_raw.start = arguments_raw.end = datetime.datetime.today()

    elif arguments_raw.end is None:
        arguments_raw.end = datetime.datetime.today()

    elif arguments_raw.end < arguments_raw.start:
        parser.error("The --end argument needs to be later than --start")

    input_arguments = InputArguments(arguments_raw.query, arguments_raw.start,
                                     arguments_raw.end, arguments_raw.output)
    return input_arguments


def output_filepath_get(filepath: str) -> str:
    """Return a valid filepath for the output csv file"""
    filepath = os.path.expanduser(filepath)

    if not filepath:
        output_file = tempfile.NamedTemporaryFile()
        return output_file.name

    if os.path.isdir(filepath):
        logger.error(
            "The specified output is a folder, not a valid file: " + filepath)
        sys.exit(4)

    if os.path.isfile(filepath):
        if not os.access(filepath, os.W_OK):
            logger.error("Cannot write to " + filepath)
            sys.exit(3)
        return filepath

    dirname = os.path.dirname(filepath)
    if not os.path.isdir(dirname):
        logger.error("Filepath not in valid folder: " + dirname)
        sys.exit(6)

    if not os.access(dirname, os.W_OK):
        logger.error("Cannot create file in: " + dirname)
        sys.exit(3)

    return filepath


# ####################################################
# Handle queries
# ####################################################

def get_query(filename: str) -> str:
    """Return the query from file."""
    query_dir = Path(__file__).parent / 'queries'
    return (query_dir / filename).read_text()


def get_open_diffs(connection: sqlite3.Connection,
                   date: datetime.datetime,
                   is_community: bool,
                   ) -> int:
    """Return all open diffs, that includes need review and planned changes."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("open-diffs.sql")
        cursor.execute(query,
                       {
                           "Date": date.strftime("%Y-%m-%d"),
                           "IsCommunity": is_community,
                       })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is not None:
            return total
    return 0


def get_open_diffs_extra(connection: sqlite3.Connection,
                         date: datetime.datetime,
                         is_draft: bool,
                         is_community: bool,
                         ) -> int:
    """Return open diffs based on their status."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("open-diffs-extra.sql")
        cursor.execute(query,
                       {
                           "Date": date.strftime("%Y-%m-%d"),
                           "StatusDraft": is_draft,
                           "StatusNeedsReview": not is_draft,
                           "IsCommunity": is_community,
                       })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is not None:
            return total
    return 0


def get_closed_diffs(connection: sqlite3.Connection,
                     date: datetime.datetime,
                     is_community: bool,
                     ) -> int:
    """Return all the diffs closed (abandoned, closed)."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("closed.sql")
        cursor.execute(query,
                       {
                           "Date": date.strftime("%Y-%m-%d"),
                           "IsCommunity": is_community,
                       })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is None:
            return 0
    return total


def get_closed_diffs_extra(
    connection: sqlite3.Connection,
    date: datetime.datetime,
    is_abandoned: bool,
    is_merged: bool,
    is_community: bool,
) -> int:
    """Return all the diffs closed (abandoned, closed)."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("closed-extra.sql")
        cursor.execute(query,
                       {
                           "Date": date.strftime("%Y-%m-%d"),
                           "StatusAbandoned": is_abandoned,
                           "StatusMerged": is_merged,
                           "IsCommunity": is_community,
                       })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is None:
            return 0
    return total


def get_new_diffs(connection: sqlite3.Connection,
                  date: datetime.datetime,
                  is_moderator: bool,
                  is_community: bool) -> int:
    """Return all the new diffs."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("new-diffs.sql")
        cursor.execute(query,
                       {
                           "Date": date.strftime("%Y-%m-%d"),
                           "AuthorIsModerator": is_moderator,
                           "AuthorIsCommunity": is_community,
                       })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is None:
            return 0
    return total


def main() -> None:
    setup_logging(logger, 'log-diff-plot.txt')
    arguments = process_arguments()
    csv_output = output_filepath_get(arguments.output)

    with closing(sqlite3.connect(str(data_dir / 'diffs.sqlite'))) as connection:
        day = arguments.date_start
        last_day = arguments.date_end
        delta = datetime.timedelta(days=1)

        logger.debug("Output file: " + csv_output)
        open(csv_output, 'w', newline='').write("")

        while day <= last_day:
            today = Day(day, arguments.query)

            logger.debug("Processing date: {date}".format(
                date=day.strftime("%Y-%m-%d")))

            # All the computation happens here
            today.process(connection)

            # Output, one open at a time to facilitate parse the output continuously
            with open(csv_output, 'a+', newline='') as csv_file:
                csv.writer(csv_file).writerow(today)
            logger.info(today)

            # Tomorrow
            day += delta


if __name__ == "__main__":
    main()
