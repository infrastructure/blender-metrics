WITH
/*
INPUT AS ( SELECT
    date("2023-01-18") AS today,
    date("2023-01-18", "+1 day") AS tomorrow,
    0 AS _abandoned,
    1 AS _merged,
    1 AS is_community
)
*/
INPUT AS ( SELECT
    :Date AS today,
    date(:Date, "+1 day") AS tomorrow,
    :StatusAbandoned AS _abandoned,
    :StatusMerged AS _merged,
    :IsCommunity AS is_community
)
SELECT COUNT(*) FROM (

SELECT

(
    SELECT
        last_status_event.status
    FROM
    (
		SELECT
			issues.date_created AS date,
            CASE
                WHEN issues.created_as_draft = 1
                THEN 'draft'
            ELSE
                'open'
            END AS status
		UNION ALL
        SELECT
            t.date_created as date,
            "merged" as status
        FROM transactions_merge_pull tm
            LEFT JOIN transactions t ON tm.transaction_id = t.id
            WHERE t.issue_number = issues.number
        UNION ALL
        SELECT
            t.date_created as date,
            "abandoned" as status
        FROM transactions_close tc
            LEFT JOIN transactions t ON tc.transaction_id = t.id
            WHERE t.issue_number = issues.number
        ORDER BY
            date DESC
        LIMIT 1
    ) AS last_status_event
) AS status_at_point_in_time

FROM
    issues,
    INPUT
WHERE
    issues.date_closed is not NULL
    AND issues.date_closed >= INPUT.today
    AND issues.date_closed < INPUT.tomorrow
    AND
        ((NOT INPUT.is_community) OR
            issues.author_id NOT in (SELECT * FROM moderators)
        )
    AND (
        (INPUT._abandoned AND status_at_point_in_time = 'abandoned') OR
        (INPUT._merged AND status_at_point_in_time = 'merged')
    )
) -- COUNT
;