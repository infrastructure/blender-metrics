SELECT count(*) FROM issues
WHERE
    issues.date_closed is not NULL
    AND issues.date_closed >= :Date
    AND issues.date_closed < date(:Date, '+1 day')
    AND
        ((NOT :IsCommunity) OR
            issues.author_id NOT in (SELECT * FROM moderators)
        )
;