SELECT count(*) FROM issues
WHERE
    issues.date_created >= :Date
    AND issues.date_created < date(:Date, '+1 day')
    AND (
        (:AuthorIsModerator AND :AuthorIsCommunity) OR
        (:AuthorIsModerator AND
            (issues.author_id in (SELECT * FROM moderators))
         ) OR
        (:AuthorIsCommunity AND
            (issues.author_id not in (SELECT * FROM moderators))
        )
    )
;