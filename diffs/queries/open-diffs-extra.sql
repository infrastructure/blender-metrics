WITH
/*
INPUT AS ( SELECT
    date("2020-05-31", "+1 day") AS tomorrow,
    0 AS draft,
    1 AS needs_review,
    1 AS is_community
)
*/
INPUT AS ( SELECT
    date(:Date, "+1 day") AS tomorrow,
    :StatusDraft AS _draft,
    :StatusNeedsReview AS needs_review,
    :IsCommunity AS is_community
)
SELECT COUNT(*) FROM (

SELECT

(
    SELECT
        last_status_event.status
    FROM
    (
		SELECT
			issues.date_created AS date,
            CASE
                WHEN issues.created_as_draft = 1
                THEN 'draft'
            ELSE
                'open'
            END AS status
		UNION ALL
        SELECT
            t.date_created as date,
            "closed" as status
        FROM transactions_merge_pull tm
            LEFT JOIN transactions t ON tm.transaction_id = t.id
            WHERE t.issue_number = issues.number
        UNION ALL
        SELECT
            t.date_created as date,
            "closed" as status
        FROM transactions_close tc
            LEFT JOIN transactions t ON tc.transaction_id = t.id
            WHERE t.issue_number = issues.number
        ORDER BY
            date DESC
        LIMIT 1
    ) AS last_status_event
) AS status_at_point_in_time

FROM
    issues,
    INPUT
WHERE
    issues.date_created < INPUT.tomorrow
    AND (issues.date_closed is NULL OR issues.date_closed >= INPUT.tomorrow)
    AND
        ((NOT INPUT.is_community) OR
            issues.author_id NOT in (SELECT * FROM moderators)
        )
    AND (
        (INPUT.needs_review AND status_at_point_in_time = 'open') OR
        (INPUT._draft AND status_at_point_in_time = 'draft')
    )
) -- COUNT
;