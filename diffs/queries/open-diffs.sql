SELECT count(*) FROM issues
WHERE
    issues.date_created < date(:Date, '+1 day')
    AND (issues.date_closed is NULL OR issues.date_closed >= date(:Date, '+1 day'))
    AND ((NOT :IsCommunity) OR
         issues.author_id NOT in (SELECT * FROM moderators)
        )
;