#!/usr/bin/env python3.8

"""Fetch and store Tasks and Transactions from developer.blender.org.

Needs to be run as module with `python -m tasks.fetch_and_store`.
This script allows to query the API (Conduit) of the Blender
development portal and fetch Tasks and their relative transactions
for the BF-Blender project. The data is progressively fetched and
saved into a local sqlite database, for more efficient processing
and data mining through other scripts.

This script requires the environment variable CONDUIT_API_TOKEN to
be set to a valid token.

The script works as follows:

    * main() is the entry point
    * fetch tasks from the server starting from `after`
        * fetch the transactions for each task
        * save task its transactions in the sqlite database
"""

import dataclasses as dc
import datetime
import logging
import os
import sqlite3
import pandas
import sys
from contextlib import closing
from typing import (
    Callable,
    Dict,
    Iterable,
    List,
    NewType,
    Optional,
    Tuple,
    TypeVar,
    cast,
)
import csv
import yarl
import itertools
from more_itertools import chunked
from pathlib import Path

from common.classes import *
from common.defines import MIGRATION
from common.utils import (
    setup_logging,
    data_dir,
    get_date_object,
)
from common.fetch import (
    fetch_issues,
    fetch_moderators,
    fetch_transactions,
)
from common.sql import (
    get_max_date_modified,
    init_sqlite,
    store_issue_and_transactions,
)
from common.store import store_moderators
from common.settings import *

logger = logging.getLogger(__file__)



@dc.dataclass
class Task(Issue):
    api_type = "issues"
    # priority: str

    def __init__(self, **kwargs):
        Issue.__init__(
            self,
            api_type=self.api_type,
            created_as_draft=False,
            **kwargs,
        )


def calculate_transaction_id(
    task_id: int,
    iterator_id: int,
) -> int:
    """
    Returns a unique negative id based on the task id, the current label.
    It assumes 200,000 maximum items
    """
    return -(task_id + (iterator_id * 200000))


def get_migration_database_path() -> str:
    return str(data_dir / 'migration.sqlite')


def create_migration_database():
    """
    Creates a database with all the tasks stats at migration time.
    """
    delete_migration_database()

    with closing(sqlite3.connect(get_migration_database_path())) as connection:
        stats = pandas.read_csv(
            Path(__file__).parent /
            'task-stats-on-migration.csv')
        stats.to_sql('stats', connection, if_exists='replace', index=False)


def delete_migration_database():
    try:
        os.remove(get_migration_database_path())
    except OSError:
        pass


def get_transactions_from_csv(
    task: Task,
    task_number: int,
) -> Iterable[Transaction]:
    """
    Return the state of the task at the time of the gitea migration
    as transactions
    """
    with closing(sqlite3.connect(get_migration_database_path())) as connection:
        cursor = connection.cursor()

        cursor.execute('''SELECT task_number, subtype, status_name, priority_name, dateCreated, dateClosed from stats WHERE task_number=:task_number''',
            {
                'task_number': task_number,
            })

        result = cursor.fetchall()
        if not result:
            return []

        task_id, subtype, status_name, priority_name, dateCreated, dateClosed = result[0]

        lookup_labels = {
            'default': 'Type/Report',
            'bug': 'Type/Bug',
            'knownissue': 'Type/Known Issue',
            'Archived': 'Status/Archived',
            'Confirmed': 'Status/Confirmed',
            'Duplicate': 'Status/Duplicate',
            'Needs Information from Developers': 'Status/Needs Information from Developers',
            'Needs Information from User': 'Status/Needs Information from User',
            'Needs Triage': 'Status/Needs Triage',
            'Resolved': 'Status/Resolved',
            'Low': 'Severity/Low',
            'Normal': 'Severity/Normal',
            'High': 'Severity/High',
            'Unbreak Now!': 'Severity/Unbreak Now!',
        }

        for i, field in enumerate((subtype, status_name, priority_name)):
            label = lookup_labels.get(field)

            if not label:
                continue

            yield LabelTransaction(
                id=calculate_transaction_id(task_number, i),
                issue_number=task_number,
                is_set=True,
                label=label,
                author_id=MIGRATION.STANDIN_AUTHOR,
                date_created=dateCreated,
                date_modified=MIGRATION.DATE,
            )

            if field in {'Archived', 'Duplicate', 'Resolved'} and dateClosed is None:
                # This was likely closed before the migration to Phabricator
                # but we don't have this information (e.g., #36323)
                task.date_closed = MIGRATION.DATE
                task.state = 'closed'

            # Although the type/status/severity can change in the life-cycle of an issue
            # They are useful for the response_time queries.
            if label.startswith("Status/"):
                task.label_status = label
            elif label.startswith("Type/"):
                task.label_type = label
            elif label.startswith("Severity/"):
                task.label_severity = label

    return []


def fetch_tasks(
    api_url: yarl.URL,
    api_token: str,
    repository_owner: str,
    repository_name: str,
    after: Optional[datetime.datetime] = None,
) -> Iterable[Task]:
    """Query API for all tasks that changed since "after".

    Yields:
        Task objects.
    """
    return fetch_issues(**locals(), IteratorClass=Task)


def main() -> None:
    api_token = os.environ['GITEA_API_TOKEN']

    data_dir.mkdir(exist_ok=True)

    setup_logging(logger, 'log-task.txt')

    gitea_domain = GITEA_DOMAIN
    api_url = yarl.URL(gitea_domain + 'api/v1/')
    organization_name = ORGANIZATION_NAME
    repository_owner = REPOSITORY_OWNER
    repository_name = REPOSITORY_NAME
    moderators_team_name = MODERATORS_TEAM_NAME

    create_migration_database()

    with closing(sqlite3.connect(str(data_dir / 'tasks.sqlite'))) as connection:
        init_sqlite(connection)

        after: Optional[datetime.datetime]
        if len(sys.argv) > 1 and sys.argv[1]:
            after = get_date_object(sys.argv[1])
        else:
            after = get_max_date_modified(connection)

        if after is not None:
            logger.info(f'Fetching updates after: {after.isoformat(sep=" ")}')

        for i, task in enumerate(
            fetch_tasks(api_url, api_token, repository_owner,
                        repository_name, after=after)
        ):
            logger.info("Iterating over task.number=" + str(task.number))
            transactions_migration = get_transactions_from_csv(task, task_number=task.number)
            transactions_gitea = fetch_transactions(
                api_url,
                api_token,
                repository_owner,
                repository_name,
                issue_number=task.number,
            )

            transactions = itertools.chain(transactions_migration, transactions_gitea)
            store_issue_and_transactions(task, transactions, connection)

            if i % 10 == 0:
                current_after = task.date_modified

                if current_after is None:
                    logger.info(f'Fetched {i + 1} tasks.')
                else:
                    logger.info(
                        f'Fetched {i + 1} tasks. We now have all modifications after: {current_after.isoformat(sep=" ")}'
                    )

        # Get a new moderators list every time
        logger.info(f'Fetching moderators list')
        moderators = fetch_moderators(
            api_url, api_token, organization_name, moderators_team_name)
        store_moderators(moderators, connection)

    delete_migration_database()


if __name__ == '__main__':
    main()
