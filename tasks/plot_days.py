#!/usr/bin/env python3.8

from argparse import ArgumentParser
import dataclasses as dc
import datetime
import git
import logging
import sqlite3
import sys
import tempfile
import csv
import os
from contextlib import closing
from pathlib import Path
from typing import List, Optional
from enum import Enum

from common.utils import assert_cast_optional, setup_logging, data_dir, T, blender_git_repository_get

logger = logging.getLogger(__file__)


@dc.dataclass
class InputArguments:
    query: Enum
    date_start: datetime.datetime
    date_end: datetime.datetime
    output: str
    git_repository: str


class Query(Enum):
    HIGH_BUGS = 'high_bugs'
    HIGH_REPORTS = 'high_reports'
    HIGH_ALL = 'high_all'
    NORMAL_BUGS = 'normal_bugs'
    LOW_BUGS = 'low_bugs'
    UNTRIAGED_BUGS = 'untriaged'
    NEED_DEVELOPER_INFO = 'need_dev_info'
    NEW_BUGS = 'new'
    OPEN_BUGS = 'open'
    UNCLASSIFIED = 'unclassified'
    KNOWN_ISSUES = 'known_issues'
    COMMITTED_FIX = 'committed_fix'
    COMMITTED_ANY = 'committed_any'
    C_FILES = 'c_files'
    DEBUG_RESET = 'reset'

    def __str__(self):
        return self.value


@dc.dataclass
class Task:
    pass


@dc.dataclass
class Day:
    date: datetime.datetime
    query: Enum
    git_repository: Optional[str]
    total: int = 0

    def __str__(self):
        return "{date};{total};".format(date=self.date.strftime("%Y-%m-%d"), total=self.total)

    def __iter__(self):
        yield from (self.date.strftime("%Y-%m-%d"), self.total)

    def process(self, connection: sqlite3.Connection) -> None:
        if self.query == Query.HIGH_BUGS:
            self.total = get_open_tasks(
                connection,
                self.date,
                is_confirmed=True,
                is_needs_triage=False,
                is_needs_user_info=False,
                is_needs_dev_info=False,
                is_bug=True,
                is_report=False,
                is_known_issue=False,
                is_unbreak=True,
                is_high=True,
                is_normal=False,
                is_low=False,
            )
        elif self.query == Query.HIGH_REPORTS:
            self.total = get_open_tasks(
                connection,
                self.date,
                is_confirmed=True,
                is_needs_triage=False,
                is_needs_user_info=False,
                is_needs_dev_info=False,
                is_bug=False,
                is_report=True,
                is_known_issue=False,
                is_unbreak=True,
                is_high=True,
                is_normal=False,
                is_low=False,
            )
        elif self.query == Query.HIGH_ALL:
            self.total = get_open_tasks(
                connection,
                self.date,
                is_confirmed=True,
                is_needs_triage=False,
                is_needs_user_info=False,
                is_needs_dev_info=False,
                is_bug=True,
                is_report=True,
                is_known_issue=False,
                is_unbreak=True,
                is_high=True,
                is_normal=False,
                is_low=False,
            )
        elif self.query == Query.NORMAL_BUGS:
            self.total = get_open_tasks(
                connection,
                self.date,
                is_confirmed=True,
                is_needs_triage=False,
                is_needs_user_info=False,
                is_needs_dev_info=False,
                is_bug=True,
                is_report=False,
                is_known_issue=False,
                is_unbreak=False,
                is_high=False,
                is_normal=True,
                is_low=False,
            )
        elif self.query == Query.LOW_BUGS:
            self.total = get_open_tasks(
                connection,
                self.date,
                is_confirmed=True,
                is_needs_triage=False,
                is_needs_user_info=False,
                is_needs_dev_info=False,
                is_bug=True,
                is_report=False,
                is_known_issue=False,
                is_unbreak=False,
                is_high=False,
                is_normal=False,
                is_low=True,
            )
        elif self.query == Query.UNTRIAGED_BUGS:
            self.total = get_open_tasks(
                connection,
                self.date,
                is_confirmed=False,
                is_needs_triage=True,
                is_needs_user_info=False,
                is_needs_dev_info=False,
                is_bug=True, # TODO should do this based on 2024-08-14
                is_report=True,
                is_known_issue=False,
                is_unbreak=True,
                is_high=True,
                is_normal=True,
                is_low=True,
            )
        elif self.query == Query.NEED_DEVELOPER_INFO:
            self.total = get_open_tasks(
                connection,
                self.date,
                is_confirmed=False,
                is_needs_triage=False,
                is_needs_user_info=False,
                is_needs_dev_info=True,
                is_bug=True,
                is_report=True,
                is_known_issue=True,
                is_unbreak=True,
                is_high=True,
                is_normal=True,
                is_low=True,
            )
        elif self.query == Query.OPEN_BUGS:
            self.total = get_open_tasks(
                connection,
                self.date,
                is_confirmed=True,
                is_needs_triage=False,
                is_needs_user_info=False,
                is_needs_dev_info=False,
                is_bug=True,
                is_report=False,
                is_known_issue=False,
                is_unbreak=True,
                is_high=True,
                is_normal=True,
                is_low=True,
            )
        elif self.query == Query.UNCLASSIFIED:
            self.total = get_open_tasks(
                connection,
                self.date,
                is_confirmed=True,
                is_needs_triage=False,
                is_needs_user_info=False,
                is_needs_dev_info=False,
                is_bug=False,
                is_report=True,
                is_known_issue=False,
                is_unbreak=True,
                is_high=True,
                is_normal=True,
                is_low=True,
            )
        elif self.query == Query.KNOWN_ISSUES:
            self.total = get_open_tasks(
                connection,
                self.date,
                is_confirmed=True,
                is_needs_triage=True,
                is_needs_user_info=True,
                is_needs_dev_info=True,
                is_bug=False,
                is_report=False,
                is_known_issue=True,
                is_unbreak=True,
                is_high=True,
                is_normal=True,
                is_low=True,
            )
        elif self.query == Query.NEW_BUGS:
            self.total = get_new_tasks(
                connection,
                self.date,
            )
        elif self.query == Query.COMMITTED_FIX:
            self.total = get_committed_fix(self.git_repository, self.date)
        elif self.query == Query.COMMITTED_ANY:
            self.total = get_committed_any(self.git_repository, self.date)
        elif self.query == Query.C_FILES:
            self.total = get_c_files(self.git_repository, self.date)
        else:
            logger.error("query type not fully implemented: %s" % (self.query,))


def process_arguments() -> InputArguments:
    """Mode is mandatory, date range is optional."""
    parser = ArgumentParser(
        description='''
    Count tracker items for a range of days.
    By default will get today's data, for tasks that need developers attention.
    '''
    )
    parser.add_argument(
        'query',
        type=Query,
        choices=list(Query),
        help='''
        query parameters: need developer attention (default), bugs, open reports, new reports or need triaging
        ''',
    )
    parser.add_argument(
        '--start',
        type=datetime.datetime.fromisoformat,
        help='''
        first day of range, today as default
        ''',
    )
    parser.add_argument(
        '--end',
        type=datetime.datetime.fromisoformat,
        help='''
        last day of range, optional, today as default
        ''',
    )
    parser.add_argument(
        '--output',
        type=str,
        default="",
        help='''
        optional output csv file
        ''',
    )
    parser.add_argument(
        '--git-repository',
        type=str,
        default="",
        help='''
        optional git repository for the 'committedfix' query
        ''',
    )

    arguments_raw = parser.parse_args()

    if arguments_raw.start is None:
        if arguments_raw.end is not None:
            parser.error("The --end argument can only be used when --start is specified")
        else:
            arguments_raw.start = arguments_raw.end = datetime.datetime.today()

    elif arguments_raw.end is None:
        arguments_raw.end = datetime.datetime.today()

    elif arguments_raw.end < arguments_raw.start:
        parser.error("The --end argument needs to be later than --start")

    if arguments_raw.query in (Query.COMMITTED_FIX, Query.COMMITTED_ANY, Query.C_FILES) and \
            not arguments_raw.git_repository:
        parser.error("The '{query}' query requires a --git-repository argument".format(query=arguments_raw.query))

    input_arguments = InputArguments(arguments_raw.query, arguments_raw.start,
                                     arguments_raw.end, arguments_raw.output, arguments_raw.git_repository)
    return input_arguments


def output_filepath_get(filepath: str) -> str:
    """Return a valid filepath for the output csv file"""
    filepath = os.path.expanduser(filepath)

    if not filepath:
        output_file = tempfile.NamedTemporaryFile()
        return output_file.name

    if os.path.isdir(filepath):
        logger.error("The specified output is a folder, not a valid file: " + filepath)
        sys.exit(4)

    if os.path.isfile(filepath):
        if not os.access(filepath, os.W_OK):
            logger.error("Cannot write to " + filepath)
            sys.exit(3)
        return filepath

    dirname = os.path.dirname(filepath)
    if not os.path.isdir(dirname):
        logger.error("Filepath not in valid folder: " + dirname)
        sys.exit(6)

    if not os.access(dirname, os.W_OK):
        logger.error("Cannot create file in: " + dirname)
        sys.exit(3)

    return filepath


# ####################################################
# Handle queries
# ####################################################


def get_query(filename: str) -> str:
    """Return the query from file."""
    query_dir = Path(__file__).parent / 'queries'
    return (query_dir / filename).read_text()


def get_open_tasks(connection: sqlite3.Connection,
                  date: datetime.datetime,
                  is_confirmed: bool,
                  is_needs_triage: bool,
                  is_needs_user_info: bool,
                  is_needs_dev_info: bool,
                  is_bug: bool,
                  is_report: bool,
                  is_known_issue: bool,
                  is_unbreak: bool,
                  is_high: bool,
                  is_normal: bool,
                  is_low: bool,
                  ) -> int:
    """Return all open tasks."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("uber.sql")
        cursor.execute(query,
                       {
                           "Date": date.strftime("%Y-%m-%d"),
                           "StatusConfirmed": is_confirmed,
                           "StatusNeedsTriage": is_needs_triage,
                           "StatusNeedsUserInfo": is_needs_user_info,
                           "StatusNeedsDevInfo": is_needs_dev_info,
                           "TypeIsBug": is_bug,
                           "TypeIsReport": is_report,
                           "TypeIsKnownIssue": is_known_issue,
                           "PriorityUnbreak": is_unbreak,
                           "PriorityHigh": is_high,
                           "PriorityNormal": is_normal,
                           "PriorityLow": is_low,
                       })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is None:
            return 0
    return total


def get_new_tasks(connection: sqlite3.Connection,
                  date: datetime.datetime,
                  ) -> int:
    """Return all new tasks."""
    cursor: sqlite3.Cursor
    with closing(connection.cursor()) as cursor:
        query = get_query("new-reports.sql")
        cursor.execute(query,
                       {
                           "Date": date.strftime("%Y-%m-%d"),
                       })

        result = cursor.fetchall()
        total = assert_cast_optional(int, result[0][0])
        if total is None:
            return 0
    return total


def get_committed_fix(git_repository: str, date: datetime.datetime) -> int:
    """Return all the reports of type bug closed as fixed."""
    import git
    repository = git.Git(git_repository)
    log_info = repository.log(
        '--before=' + (datetime.timedelta(days=1) + date).strftime("%Y-%m-%d") + ' 00:00',
        '--after=' + date.strftime("%Y-%m-%d") + ' 00:00',
        '--date=local',
        '--oneline',
        '--pretty=format:%s',
        '--no-merges',
    ).split('\n')

    return len([i for i in log_info if "fix" in i.lower()])


def get_committed_any(git_repository: str, date: datetime.datetime) -> int:
    """Return all the commits in a day."""
    import git
    repository = git.Git(git_repository)
    log_info = repository.log(
        '--before=' + (datetime.timedelta(days=1) + date).strftime("%Y-%m-%d") + ' 00:00',
        '--after=' + date.strftime("%Y-%m-%d") + ' 00:00',
        '--date=local',
        '--oneline',
        '--pretty=format:%s',
        '--no-merges',
    ).split('\n')

    return len(log_info)


def get_c_files(git_repository: str, date: datetime.datetime) -> int:
    """Return all the c files in source/blender."""
    import os
    import subprocess
    git_date = date.strftime('%Y-%m-%d')

    subfolder = 'source/blender'

    # The Git command to get the commit hash for the given date
    git_command = f'git rev-list -1 --first-parent --before {git_date} origin/main'

    # Get the commit hash for the given date
    try:
        commit_hash = subprocess.check_output(git_command, shell=True, cwd=git_repository, universal_newlines=True).strip()
    except subprocess.CalledProcessError as e:
        print(f'Error: {e}')
        exit()

    # Use Git to get a list of all .c files in the repository at the specified date
    git_command = f'git ls-tree --name-only -r "{commit_hash}" {subfolder} | grep "\.c$"'
    try:
        c_files = subprocess.check_output(git_command, shell=True, cwd=git_repository, universal_newlines=True)
        c_file_list = c_files.split('\n')
        c_file_count = len(c_file_list) - 1  # Subtract 1 to exclude the last empty line
    except subprocess.CalledProcessError as e:
        print(f'Error: {e}')

    logger.debug(f'The number of .c files in {git_repository} is {c_file_count}')
    return c_file_count


def main() -> None:
    setup_logging(logger, 'log-task-plot.txt')
    arguments = process_arguments()
    csv_output = output_filepath_get(arguments.output)
    git_repository = blender_git_repository_get(arguments.git_repository)

    with closing(sqlite3.connect(str(data_dir / 'tasks.sqlite'))) as connection:

        day = arguments.date_start
        last_day = arguments.date_end
        delta = datetime.timedelta(days=1)

        logger.debug("Output file: " + csv_output)
        open(csv_output, 'w', newline='').write("")

        while day <= last_day:
            today = Day(day, arguments.query, git_repository)

            logger.debug("Processing date: {date}".format(date=day.strftime("%Y-%m-%d")))

            # All the computation happens here
            today.process(connection)

            # Output, one open at a time to facilitate parse the output continuously
            with open(csv_output, 'a+', newline='') as csv_file:
                csv.writer(csv_file).writerow(today)
            logger.info(today)

            # Tomorrow
            day += delta


if __name__ == "__main__":
    main()
