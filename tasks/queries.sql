CREATE INDEX IF NOT EXISTS tasks__date_created__date_closed__phid ON tasks (date_created, date_closed, phid);
CREATE INDEX IF NOT EXISTS transactions__task_phid__date_created__id ON transactions (task_phid, date_created, id);
CREATE INDEX IF NOT EXISTS transactions_status__transaction_id__new ON transactions_status (transaction_id, new);
CREATE INDEX IF NOT EXISTS transactions_subtype__transaction_id__new ON transactions_subtype (transaction_id, new);

ANALYZE main;

EXPLAIN QUERY PLAN
WITH RECURSIVE point_in_time(date) AS (
        VALUES ('2020-03-01')
        UNION ALL
        SELECT date(date, '+1 day')
        FROM point_in_time
        WHERE date(date, '+1 day') <= date('2020-04-17')
)
SELECT
        point_in_time.date,
        (
        SELECT
                last_status_event.status
        FROM (
                /*
                Set an initial status value, and on top of it add all the
                status change transactions up to the specified date.
                We limit it to `1` to get only the latest status.
                */
                SELECT
                        tasks.date_created AS date,
                        'needstriage'      AS status,
                        2                  AS _order
                        WHERE
                                tasks.date_created <= point_in_time.date
                UNION ALL
                SELECT
                        t.date_created AS date,
                        'duplicate'    AS status,
                        1              AS _order
                        FROM transactions_mergedinto tm
                        LEFT JOIN transactions t ON tm.transaction_id = t.id
                        WHERE
                                t.task_phid = tasks.phid
                                AND t.date_created <= point_in_time.date
                UNION ALL
                SELECT
                        t.date_created AS date,
                        ts.new         AS status,
                        0              AS _order
                        FROM transactions_status ts
                        LEFT JOIN transactions t ON ts.transaction_id = t.id
                        WHERE
                                t.task_phid = tasks.phid
                                AND t.date_created <= point_in_time.date
                ORDER BY
                        date DESC,
                        _order
                LIMIT 1
        ) last_status_event) AS status_at_point_in_time,
        (
        SELECT
                last_subtype_event.subtype
        FROM (
                SELECT
                        tasks.date_created AS date,
                        'default'          AS subtype,
                        1                  AS _order
                        WHERE
                                tasks.date_created <= point_in_time.date
                UNION ALL
                SELECT
                        t.date_created AS date,
                        ts.new         AS subtype,
                        0              AS _order
                        FROM transactions_subtype ts
                        LEFT JOIN transactions t ON ts.transaction_id = t.id
                        WHERE
                                t.task_phid = tasks.phid
                                AND t.date_created <= point_in_time.date
                ORDER BY
                        date DESC,
                        _order
                LIMIT 1
        ) last_subtype_event) AS subtype_at_point_in_time,
        count(*) AS count
FROM point_in_time,
       tasks
WHERE
        tasks.date_created <= point_in_time.date
        AND (
                tasks.date_closed IS NULL OR
                point_in_time.date < tasks.date_closed
        )
        AND tasks.subtype IN ('default', 'bug', 'known_issue')
        AND subtype_at_point_in_time IN ('default', 'bug')
        AND status_at_point_in_time IN ('open', 'confirmed', 'needstriage', 'needsdevelopertoreproduce')
GROUP BY
        point_in_time.date,
        status_at_point_in_time,
        subtype_at_point_in_time
ORDER BY point_in_time.date, status_at_point_in_time DESC, subtype_at_point_in_time DESC;
