SELECT count(*) FROM tasks
WHERE
    tasks.date_closed >= :Date
    AND tasks.date_closed < date(:Date, '+1 day')
    AND ((tasks.subtype = 'default' /* aka report */ AND :Date < :Xmas)
         OR (tasks.subtype IN ('knownissue', 'bug')))
    AND tasks.status = 'resolved';