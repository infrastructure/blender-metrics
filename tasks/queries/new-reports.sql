
WITH

/*
INPUT AS ( SELECT
    date("2023-01-19") AS today,
    date("2023-01-19", "+1 day") AS tomorrow
)
*/

INPUT AS ( SELECT
    date(:Date) AS today,
    date(:Date, "+1 day") AS tomorrow
)

SELECT COUNT(*) FROM (

SELECT
    INPUT.today,
    issue_number,
    CASE
        WHEN bug_is_set IS NULL THEN 0
        ELSE bug_is_set
    END AS _bug_is_set,
    CASE
        WHEN report_is_set IS NULL THEN 0
        ELSE report_is_set
    END AS _report_is_set,
    CASE
        WHEN known_issue_is_set IS NULL THEN 0
        ELSE known_issue_is_set
    END AS _known_issue_is_set

FROM
(
    SELECT
    issues.number as issue_number,
    bug_is_set, report_is_set, known_issue_is_set

    FROM

    issues

    LEFT JOIN
    (
    SELECT i.number as bug_issue_number, t.is_set as bug_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Type/Bug'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Type/Bug'
    )
    ON issues.number = bug_issue_number

    LEFT JOIN
    (
    SELECT i.number as report_issue_number, t.is_set as report_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Type/Report'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Type/Report'
    )
    ON issues.number = report_issue_number

    LEFT JOIN
    (
    SELECT i.number as known_issue_number, t.is_set as known_issue_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Type/Known Issue'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Type/Known Issue'
    )
    ON issues.number = known_issue_number

)
info_at_point,
issues,
INPUT

WHERE
    issues.number = info_at_point.issue_number

    AND
    issues.date_created >= INPUT.today AND
    issues.date_created < INPUT.tomorrow

    -- type
    AND ((
    _bug_is_set) OR (
    _report_is_set) OR (
    _known_issue_is_set))

) -- COUNT
;
