WITH INPUT AS (SELECT
  -- CAST(strftime("%s", datetime("2023-01-01 03:58:25+01:00")) as INTEGER) as date_from,
  -- CAST(strftime("%s", datetime("2024-01-01 03:58:25+01:00")) as INTEGER) as date_to
  strftime("%s", :DateFrom) as date_from,
  strftime("%s", :DateTo) as date_to
),
 RESPONSE_TIME AS
 (
SELECT
  CAST(strftime("%s", date_created) as INTEGER) as created
  FROM INPUT, issues
  WHERE created > INPUT.date_from
  AND created <= INPUT.date_to
  AND issues.label_type IN ('Type/Bug', 'Type/Report', 'Type/Known Issue')
  AND (
    issues.label_status IN ('Status/Confirmed', 'Status/Resolved')
    OR issues.state = 'closed'
  )
  AND issues.author_id NOT IN (SELECT id FROM moderators)
)
SELECT COUNT(*) FROM (SELECT * FROM RESPONSE_TIME)