/*
SQL query to populate an secondary table to help the other queries.

There was a migration around late 2013 where some data was missing.

This rolls back the transactions to set the status, priority and closing date back in that time.

Expected input: Xmas (2019, 12, 23), Sourceforce (2013, 11, 14)
*/

WITH constants AS (SELECT
    date(:Xmas) AS xmas,
    date(:Sourceforge) AS sourceforge
)

INSERT INTO tasks_sourceforge_snapshot (task_id, status, priority)

SELECT
    tasks.id AS task_id,
    (
        SELECT
            first_status_event.status
        FROM
        (
            SELECT
                constants.xmas AS date,
                tasks.status   AS status,
                2              AS _order
                WHERE
                    tasks.date_created < constants.sourceforge
            UNION ALL
            SELECT
                t.date_created AS date,
                'normal'       AS status, --no way to find this out, arbitrarly setting to normal
                1              AS _order
                FROM transactions_mergedinto tm
                LEFT JOIN transactions t ON tm.transaction_id = t.id
                WHERE
                    t.task_phid = tasks.phid
                    AND t.date_created < constants.xmas
                    AND t.date_created > constants.sourceforge
            UNION ALL
            SELECT
                t.date_created AS date,
                ts.old         AS status,
                0              AS _order
                FROM transactions_status ts
                LEFT JOIN transactions t ON ts.transaction_id = t.id
                WHERE
                    t.task_phid = tasks.phid
                    AND t.date_created < constants.xmas
                    AND t.date_created > constants.sourceforge
            ORDER BY
                date DESC,
                _order
            LIMIT 1
        ) first_status_event
    ) AS status,
    (
        SELECT
            first_priority_event.priority
        FROM
        (
            SELECT
                constants.xmas AS date,
                tasks.priority AS priority,
                2              AS _order
                WHERE
                    tasks.date_created < constants.sourceforge
            UNION ALL
            SELECT
                t.date_created AS date,
                tp.old         AS priority,
                0              AS _order
                FROM transactions_priority tp
                LEFT JOIN transactions t ON tp.transaction_id = t.id
                WHERE
                    t.task_phid = tasks.phid
                    AND t.date_created < constants.xmas
                    AND t.date_created > constants.sourceforge
            ORDER BY
                date ASC,
                _order
            LIMIT 1
        ) first_priority_event
    ) AS priority
FROM
    tasks,
    constants
WHERE
    tasks.date_created < constants.sourceforge
    AND tasks.subtype IN (
        'default' /* aka report */,
        'knownissue',
        'bug'
    )
;