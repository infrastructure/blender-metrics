/* 
 There are a few relevant labels:
 * Severity/High  - confirmed and high (Report or Bug)
 * Type/Bug       - confirmed
 * Type/Report    - confirmed or unconfirmed
 * Type/Known Issue
 * Status/Confirmed
 * Status/Needs User Info - unconfirmed

 The other relevant info is whether the bug is in a
 project or not. Bugs that are not in a project are
 considered untriaged.

 A High Severity bug is considered a confirmed bug implicitly.
 A Needs User Info is considered a untriaged bug implicitly.
 */

WITH

/*
INPUT AS ( SELECT
    date("2023-01-19") AS today,
    date("2023-01-19", "+1 day") AS tomorrow,

    -- status
    1 AS status_confirmed,
    0 AS status_needs_triage,
    0 AS status_needs_user_info,
    0 AS status_needs_dev_info,

    -- type
    1 AS type_bug,
    0 AS type_report,
    0 AS type_known_issue,

    -- priority
    1 AS priority_unbreak,
    1 AS priority_high,
    0 AS priority_normal,
    0 AS priority_low
)
*/

    INPUT AS ( SELECT
        date(:Date) AS today,
        date(:Date, "+1 day") AS tomorrow,

        -- status
        :StatusConfirmed AS status_confirmed,
        :StatusNeedsTriage AS status_needs_triage,
        :StatusNeedsUserInfo AS status_needs_user_info,
        :StatusNeedsDevInfo AS status_needs_dev_info,

        -- type
        :TypeIsBug AS type_bug,
        :TypeIsReport AS type_report,
        :TypeIsKnownIssue AS type_known_issue,

        -- priority
        :PriorityUnbreak AS priority_unbreak,
        :PriorityHigh AS priority_high,
        :PriorityNormal AS priority_normal,
        :PriorityLow AS priority_low
    )

SELECT COUNT(*) FROM (

SELECT
    INPUT.today,
    issue_number,
    CASE
        WHEN confirmed_is_set IS NULL THEN 0
        ELSE confirmed_is_set
    END AS _confirmed_is_set,
    CASE
        WHEN need_triage_is_set IS NULL THEN 0
        ELSE need_triage_is_set
    END AS _need_triage_is_set,
    CASE
        WHEN need_user_info_is_set IS NULL THEN 0
        ELSE need_user_info_is_set
    END AS _need_user_info_is_set,
    CASE
        WHEN need_dev_info_is_set IS NULL THEN 0
        ELSE need_dev_info_is_set
    END AS _need_dev_info_is_set,
    CASE
        WHEN unbreak_priority_is_set IS NULL THEN 0
        ELSE unbreak_priority_is_set
    END AS _unbreak_priority_is_set,
    CASE
        WHEN high_priority_is_set IS NULL THEN 0
        ELSE high_priority_is_set
    END AS _high_priority_is_set,
    CASE
        WHEN normal_priority_is_set IS NULL THEN 0
        ELSE normal_priority_is_set
    END AS _normal_priority_is_set,
    CASE
        WHEN low_priority_is_set IS NULL THEN 0
        ELSE low_priority_is_set
    END AS _low_priority_is_set,
    CASE
        WHEN bug_is_set IS NULL THEN 0
        ELSE bug_is_set
    END AS _bug_is_set,
    CASE
        WHEN report_is_set IS NULL THEN 0
        ELSE report_is_set
    END AS _report_is_set,
    CASE
        WHEN known_issue_is_set IS NULL THEN 0
        ELSE known_issue_is_set
    END AS _known_issue_is_set

FROM
(
    SELECT
    issues.number as issue_number,
    confirmed_is_set, need_user_info_is_set, need_dev_info_is_set, need_triage_is_set,
    unbreak_priority_is_set, high_priority_is_set, normal_priority_is_set, low_priority_is_set,
    bug_is_set, report_is_set, known_issue_is_set

    FROM

    issues

    LEFT JOIN
    (
    SELECT i.number as confirmed_issue_number, t.is_set as confirmed_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Status/Confirmed'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Status/Confirmed'
    )
    ON issues.number = confirmed_issue_number

    LEFT JOIN 
    (
    SELECT i.number as need_user_info_issue_number, t.is_set as need_user_info_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Status/Needs Information from User'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Status/Needs Information from User'
    )
    ON issues.number = need_user_info_issue_number

    LEFT JOIN 
    (
    SELECT i.number as need_dev_info_issue_number, t.is_set as need_dev_info_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Status/Needs Information from Developers'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Status/Needs Information from Developers'
    )
    ON issues.number = need_dev_info_issue_number

    LEFT JOIN 
    (
    SELECT i.number as need_triage_issue_number, t.is_set as need_triage_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Status/Needs Triage'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Status/Needs Triage'
    )
    ON issues.number = need_triage_issue_number

    LEFT JOIN
    (
    SELECT i.number as unbreak_priority_issue_number, t.is_set as unbreak_priority_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Severity/Unbreak Now!'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Severity/Unbreak Now!'
    )
    ON issues.number = unbreak_priority_issue_number

    LEFT JOIN
    (
    SELECT i.number as high_priority_issue_number, t.is_set as high_priority_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Severity/High'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Severity/High'
    )
    ON issues.number = high_priority_issue_number

    LEFT JOIN
    (
    SELECT i.number as normal_priority_issue_number, t.is_set as normal_priority_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Severity/Normal'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Severity/Normal'
    )
    ON issues.number = normal_priority_issue_number

    LEFT JOIN
    (
    SELECT i.number as low_priority_issue_number, t.is_set as low_priority_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Severity/Low'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Severity/Low'
    )
    ON issues.number = low_priority_issue_number

    LEFT JOIN
    (
    SELECT i.number as bug_issue_number, t.is_set as bug_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Type/Bug'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Type/Bug'
    )
    ON issues.number = bug_issue_number

    LEFT JOIN
    (
    SELECT i.number as report_issue_number, t.is_set as report_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Type/Report'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Type/Report'
    )
    ON issues.number = report_issue_number

    LEFT JOIN
    (
    SELECT i.number as known_issue_number, t.is_set as known_issue_is_set
    FROM issues i
    JOIN (
        SELECT issue_number, max(date_created) as max_date
        FROM
        (
            transactions_label tl
            LEFT JOIN transactions t
            ON tl.transaction_id = t.id
        ),
            INPUT
            WHERE date_created < INPUT.tomorrow
            AND tl.label = 'Type/Known Issue'
            GROUP BY issue_number
    ) tm
    ON i.number = tm.issue_number
    JOIN (
        transactions_label tl
        LEFT JOIN transactions t
        ON tl.transaction_id = t.id
    ) t
    ON t.issue_number = i.number
    AND t.date_created = tm.max_date
    AND tl.label = 'Type/Known Issue'
    )
    ON issues.number = known_issue_number

)
info_at_point,
issues,
INPUT

WHERE
    issues.number = info_at_point.issue_number
    AND issues.date_created < INPUT.tomorrow
    AND (issues.date_closed is NULL OR issues.date_closed >= INPUT.tomorrow)

    -- priority
    AND ((
    INPUT.priority_unbreak AND _unbreak_priority_is_set) OR (
    INPUT.priority_high AND _high_priority_is_set) OR (
    INPUT.priority_normal AND _normal_priority_is_set) OR (
    INPUT.priority_low AND _low_priority_is_set))

    -- type
    AND ((
    INPUT.type_bug AND _bug_is_set) OR (
    INPUT.type_report AND _report_is_set) OR (
    INPUT.type_known_issue AND _known_issue_is_set))

    -- status
    AND ((
    INPUT.status_confirmed AND _confirmed_is_set) OR (
    INPUT.status_needs_user_info AND _need_user_info_is_set) OR (
    INPUT.status_needs_triage AND _need_triage_is_set) OR (
    INPUT.status_needs_dev_info AND _need_dev_info_is_set))


) -- COUNT
;