#!/usr/bin/env python3.8

from argparse import ArgumentParser
import os
import dataclasses as dc
import datetime
import logging
import sqlite3
from contextlib import closing
from common.utils import assert_cast, setup_logging, data_dir, blender_git_repository_get
from . import plot_days
from .plot_days import Query, get_query
from enum import Enum

logger = logging.getLogger(__file__)


@dc.dataclass
class InputArguments:
    gitrepository: str
    date_start: datetime.datetime
    date_end: datetime.datetime
    force_reset: bool


def process_arguments() -> InputArguments:
    """Mode is mandatory, date range is optional."""
    parser = ArgumentParser(
        description='''
    Count tracker items for a range of days.
    By default will get today's data, for tasks that need developers attention.
    '''
    )
    parser.add_argument(
        'gitrepository',
        type=str,
        help='''
        git repository for the git specific queries
        ''',
    )
    parser.add_argument(
        '--start',
        type=datetime.datetime.fromisoformat,
        help='''
        first day of range, yesterday as default
        ''',
    )
    parser.add_argument(
        '--end',
        type=datetime.datetime.fromisoformat,
        help='''
        last day of range, optional, yesterday as default
        ''',
    )
    parser.add_argument(
        '--force-reset',
        dest='force_reset',
        action='store_true',
        help='''
        reset the plot database
        ''',
    )
    parser.set_defaults(force_reset=False)

    arguments_raw = parser.parse_args()

    if arguments_raw.start is None:
        if arguments_raw.end is not None:
            parser.error("The --end argument can only be used when --start is specified")
        else:
            arguments_raw.start = arguments_raw.end = datetime.datetime.today() - datetime.timedelta(days=1)

    elif arguments_raw.end is None:
        arguments_raw.end = datetime.datetime.today() - datetime.timedelta(days=1)

    elif arguments_raw.end < arguments_raw.start:
        parser.error("The --end argument needs to be later than --start")

    input_arguments = InputArguments(arguments_raw.gitrepository, arguments_raw.start,
                                     arguments_raw.end, arguments_raw.force_reset)
    return input_arguments


def get_query_id(connection: sqlite3.Connection, query_name: Query) -> int:
    """Return the query id for a query name"""
    cursor: sqlite3.Cursor
    total = 0

    with closing(connection.cursor()) as cursor:
        query = get_query("plotted-queries-id.sql")
        cursor.execute(query, {
            "QueryName": query_name,
        })

        result = cursor.fetchall()
        query_id = assert_cast(int, result[0][0])

        if query_id is not None:
            return query_id

    # No query found
    raise Exception


def create_tables_scheme(connection: sqlite3.Connection):
    """Create the tables scheme for the database."""

    connection.executescript('''
    DROP TABLE IF EXISTS "queries";
    DROP TABLE IF EXISTS "values";
    ''')

    connection.executescript('''
    CREATE TABLE "queries" (
        "id"    INTEGER,
        "name"  TEXT,
        "ui_name"       INTEGER,
        "description"   INTEGER,
        PRIMARY KEY("id")
    );

    CREATE TABLE "values" (
            "id"    INTEGER,
            "date"  TEXT,
            "count" INTEGER,
            "query" INTEGER,
            PRIMARY KEY("id"),
            FOREIGN KEY("query") REFERENCES "queries"("id")
    )
    ''')


def populate_queries_table(connection: sqlite3.Connection):
    """
    Populate the queries table.
    """
    cursor: sqlite3.Cursor

    query_lookup = {
        Query.HIGH_BUGS: ("High Priority Bugs", "All the high unbreak now priority bugs"),
        Query.HIGH_REPORTS: ("High Priority Reports", "All the high and unbreak now priority reports"),
        Query.HIGH_ALL: ("High All", "All the high priority tasks (bugs or reports)"),
        Query.NORMAL_BUGS: ("Normal Priority Bugs", "All the normal priority bugs"),
        Query.LOW_BUGS: ("Low Priority Bugs", "All the low priority bugs"),
        Query.UNTRIAGED_BUGS: ("Untriaged Bugs", "Reports that may be invalid, known issues or bugs - They have the bug label but no project label"),
        Query.NEED_DEVELOPER_INFO: ("Need Developer Info", "Untriaged reports waiting for a developer to reply"),
        Query.NEW_BUGS: ("New Bugs", "New tasks with a bug label created by users (not from moderators)"),
        Query.OPEN_BUGS: ("Confirmed Bugs", "Confirmed bugs - bugs that are assigned to a project"),
        Query.UNCLASSIFIED: ("Unclassified", "Confirmed reports that are either known issues or bugs"),
        Query.KNOWN_ISSUES: ("Known Issues", "Known Issues"),
        Query.COMMITTED_FIX: ("Fix Commits", "Commits with the FIX on their headers"),
        Query.COMMITTED_ANY: ("Commits", "All the git commits (ignoring merges)"),
        Query.C_FILES: ("C Files", "How many C-Files are remaining in source/blender"),
    }

    with closing(connection.cursor()) as cursor:
        for query in Query:
            if query.value == str(Query.DEBUG_RESET):
                continue
            query_name = query.value

            full_name, description = query_lookup.get(query, ("", ""))
            sql_query = '''
            WITH INPUT AS (SELECT
                :QueryName AS name,
                :FullName AS ui_name,
                :Description AS description)

            INSERT INTO "queries" (name, ui_name, description)
            SELECT name, ui_name, description FROM INPUT;
            '''
            cursor.execute(sql_query, {
                "QueryName": query_name,
                "FullName": full_name,
                "Description": description,
            })


def insert_plotted_value(connection: sqlite3.Connection, date: datetime.datetime, count: int, query_id: int) -> bool:
    """Update the database with the new values"""
    cursor: sqlite3.Cursor
    logger.debug("Adding new value: date={date}, count={count}, query_id={query_id}".format(
        date=date.strftime("%Y-%m-%d"), count=count, query_id=query_id))

    with closing(connection.cursor()) as cursor:
        query = get_query("insert-plot-values.sql")
        cursor.execute(query, {
            "Date": date.strftime("%Y-%m-%d"),
            "Count": count,
            "QueryId": query_id,
        })

    return False


def main() -> None:
    setup_logging(logger, 'log-tasks-update-plotted-data.txt')
    arguments = process_arguments()
    git_repository = blender_git_repository_get(arguments.gitrepository)
    output_database_url = str(data_dir / 'tasks-plot.sqlite')

    if arguments.force_reset or not os.path.exists(output_database_url):
        logger.info("Creating database: " + output_database_url)
        # We need run this before the main loop to be sure the database
        # was written to before we add new data.
        with closing(sqlite3.connect(output_database_url)) as output_connection:
            create_tables_scheme(output_connection)
            populate_queries_table(output_connection)

    with closing(sqlite3.connect(output_database_url)) as output_connection:
        with closing(sqlite3.connect(str(data_dir / 'tasks.sqlite'))) as input_connection:

            day = arguments.date_start
            last_day = arguments.date_end
            delta = datetime.timedelta(days=1)

            while day <= last_day:
                # query iterator should be refactor away
                all_queries = []
                for query in Query:
                    if query.value == str(Query.DEBUG_RESET):
                        continue
                    query_id = get_query_id(output_connection, query.value)

                    today = plot_days.Day(day, query, git_repository)
                    logger.debug("Processing date: {date}".format(date=day.strftime("%Y-%m-%d")))

                    # All the computation happens here
                    today.process(input_connection)

                    # Update database
                    insert_plotted_value(output_connection, today.date, today.total, query_id)

                # Tomorrow
                day += delta


if __name__ == "__main__":
    main()
